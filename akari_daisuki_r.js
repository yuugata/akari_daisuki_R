/**
*
* あかり大好きbot R
* 
* -「わぁいうすしお　あかりうすしお大好き」をひたすら改変してツイートします．※このbotは非公式のものです。
* 
* twitter: @akari_daisuki_r
*
*/

// Load the Underscore.js library.
// key "MGwgKN2Th03tJ5OdmlzB8KPxhMjh3Sh48".
var _ = Underscore.load();

var PROPERTY_KEY = 'akari_daisuki_R';

/*

はじめにこの関数内の各APIキーを取得し直したものに置き換えて実行してください

*/
function setup() {
  var conf = {
    // Twitter API (https://apps.twitter.com/ より取得)
    TWITTER_CONSUMER_KEY     : 'Dr9Uv3aLxxxxxxxxxxxxx',
    TWITTER_CONSUMER_SECRET  : 'nmbhNbIz1cfVxxxxxxxxxxxxxxxxxxxxxxx',
    
    // Google Fusion Tables id (作成したFusionTablesのURLより https://www.google.com/fusiontables/DataSource?docid=[FUSION_TABLES_ID] )
    FUSION_TABLES_ID         : '1lgeOMxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    
    // Yahoo Application ID (https://e.developer.yahoo.co.jp/register より取得)
    YAHOO_APPLICATION_ID     : 'dj0zaiZpPxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
  };
  
  // save conf
  var value = JSON.stringify(conf);
  var propertyStore = PropertiesService.getScriptProperties();
  propertyStore.setProperty(PROPERTY_KEY, value);
  
  startAuthorize(conf.TWITTER_CONSUMER_KEY, conf.TWITTER_CONSUMER_SECRET) ;
}

/*
この関数を定期実行する
*/
function run() {
  var conf = getConfigure();
  
  var twitter = new Twitter(conf.TWITTER_CONSUMER_KEY,conf.TWITTER_CONSUMER_SECRET);
  
  // TLを取得
  var tl = twitter.getHomeTimeline({include_rts:false});
  
  tl = _.chain(tl)
    
  // メンチョンが飛ばないようにする
  .map(function(post){return post.text.replace(/@[0-9a-zA-Z_]+/g,""); })
  .shuffle()
  .value();
  
  var twiWord;
  
  // 投稿する単語を抽出  
  _.any(tl,function(text){
    try{
      var keyphrases = YahooKeyphraseExtract(conf.YAHOO_APPLICATION_ID,text);
      if(_.isEmpty(keyphrases)){throw new Error('Keyphrases not found'); }
      
      // スコアが高いものから選択
      keyphrases = _.sortBy(_.pairs(keyphrases), function(word){return - word[1];});
      
      var wordScore = _.chain(keyphrases)
      .filter(function(word){return word[1] > 5;})
      
      // これまでにツイートしていない単語を検索
      .find(function(word){
        return !existInTable(conf.FUSION_TABLES_ID,word[0]);
      })
      .value();
      
      if(_.isEmpty(wordScore)){throw new Error('Keyphrases are already posted'); }
      
      twiWord = wordScore[0];
      return true;
    }catch(e){
      Logger.log(e);
      return false;
    }    
  });
  
  if(_.isUndefined(twiWord)){
    Logger.log('keyword not found');
    return;
  }
  
  var akariTweet = 'わぁい' + twiWord + ' あかり' + twiWord + '大好き';
  twitter.update(akariTweet);
  
  // 同じ単語を投稿しないように保存する
  insertToTable(conf.FUSION_TABLES_ID,twiWord);
}

function getConfigure () {
  var propertyStore = PropertiesService.getScriptProperties();
  var obj = propertyStore.getProperty(PROPERTY_KEY);
  if (obj) {
    return JSON.parse(obj);
  } else {
    return null;
  }
}

/**
 * OAuth認証のための関数
 */

// 最初にこの関数を実行
function startAuthorize(ck,cs) {
  var twitterService = new Twitter(ck,cs).getService();
  if (!twitterService.hasAccess()) {
    var authorizationUrl = twitterService.authorize();
    Logger.log(authorizationUrl);
  }else{
    Logger.log('authorized');
  }
}

// Auth認証のコールバック
function authCallback(request) {
  var conf = getConfigure();
  var twitterService = new Twitter(conf.TWITTER_CONSUMER_KEY,conf.TWITTER_CONSUMER_SECRET).getService();
  var isAuthorized = twitterService.handleCallback(request);
  if (isAuthorized) {
    return HtmlService.createHtmlOutput('Success! You can close this tab.');
  } else {
    return HtmlService.createHtmlOutput('Denied. You can close this tab');
  }
}

/**
キーフレーズ抽出API
http://jlp.yahooapis.jp/KeyphraseService/V1/extract
*/
var YahooKeyphraseExtract = function(appid,sentence){
  var params = {
    appid    : appid,
    sentence : sentence,
    output   : 'json',
  };
  
  var url = 'http://jlp.yahooapis.jp/KeyphraseService/V1/extract' + '?' + Object.keys(params).map(function(key) {
    return encodeRfc3986(key) + '=' + encodeRfc3986( params[key]);
  }).join('&');
  var result = UrlFetchApp.fetch(url);
  return JSON.parse(result.getContentText());  
};

/**
Fusion Tables API
*/
function existInTable (id,text) {
  var sql = "SELECT Text FROM "+ id + " WHERE Text= '" + text + "' LIMIT 1";
  var res = FusionTables.Query.sqlGet(sql);
  return (res.rows && res.rows.length > 0 );
};

function insertToTable (id,text) {
  var sql = "INSERT INTO " + id + " ('Text') VALUES ('" + text + "')";
  FusionTables.Query.sql(sql);
};
