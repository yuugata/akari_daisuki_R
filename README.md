# akari_daisuki_R
「わぁいうすしお　あかりうすしお大好き」をひたすら改変してツイートします ([@akari_daisuki_R](https://twitter.com/akari_daisuki_R))

ツイートするワードはTLから拾っています

![akari_daisuki_R_twitter_icon](https://pbs.twimg.com/profile_images/3600366025/68c205f8a4bc4f8928df56f9993b2355.jpeg)

## 必要なもの

- Twitterアプリケーションキー
  - https://apps.twitter.com/ より取得
- Yahoo アプリケーションキー
  - https://e.developer.yahoo.co.jp/register より取得
- 作成したFusionTablesのtable id
  - Google Driveで「作成」→「アプリ追加」を選択､"Fusion Tables"を検索して「接続」ボタンからアプリを追加
  - 「作成」→「Fusion Tables」でFusion Tablesを作成､Create Empty Tableを選択
  - 作成したFusion TablesのURLにdocidクエリが入っているのでこれをコピペ(https://www.google.com/fusiontables/DataSource?docid=[table_id]#)


## あかり大好きbot Rの作り方

### 初回設定
1. [こちら](https://script.google.com/d/1Gx9gHxpwnGbuoWZpy98OwfAkynlMg7tB3JXd8EuQMGQLZBYmV-vzeiKt/edit?newcopy=true)から
お使いのGoogle Driveにスクリプトをコピーする
1. akari_daisuki_rファイル内に書かれているAPIキーを取得したものに書き換える

 ```js
var conf = {
  // Twitter API (https://apps.twitter.com/ より取得)
  TWITTER_CONSUMER_KEY     : 'Dr9Uv3aLxxxxxxxxxxxxx',
  TWITTER_CONSUMER_SECRET  : 'nmbhNbIz1cfVxxxxxxxxxxxxxxxxxxxxxxx',

  // Google Fusion Tables id (作成したFusionTablesのURLより https://www.google.com/fusiontables/DataSource?docid=[FUSION_TABLES_ID] )
  FUSION_TABLES_ID         : '1lgeOMxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
  
  // Yahoo Application ID (https://e.developer.yahoo.co.jp/register より取得)
  YAHOO_APPLICATION_ID     : 'dj0zaiZpPxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
};
 ```

1. setup()関数を選択し実行する
1. メニュー内の「表示」→ 「ログ(Ctrl+Enter)」より出力されたURLにアクセスしてTwitterを認証する

### Fusion Tables APIを有効にする
1. 「リソース」→「Googleの拡張サービス」で「Fusion Tables API」を有効
1. ダイアログの「Googleデベロッパーコンソール」のリンクをクリックして、「Fusion Tables API」を有効
1. run()関数を選択して「実行」すると、アプリの認証を求められるので、認証する

### 実行トリガー設定

1. 「リソース」→ 「現在のプロジェクトのトリガー」より「新しいトリガーを追加」
1. run() を実行するトリガーを作成する

![trigger](http://drive.google.com/uc?export=view&id=0BxQFVoGrWK1pWGV3TUNUSnY0a1k)

## 開発

> Fork､改変ご自由にどうぞ

> 手軽で無料なGASで最強のbotをつくろう
