/**
Twitter
*/
var Twitter = function (consumerKey, consumerSecret) {
  var properties = PropertiesService.getScriptProperties();
  
  this.service = OAuth1.createService('twitter')
  // Set the endpoint URLs.
  .setAccessTokenUrl('https://api.twitter.com/oauth/access_token')
  .setRequestTokenUrl('https://api.twitter.com/oauth/request_token')
  .setAuthorizationUrl('https://api.twitter.com/oauth/authorize')
  
  // Set the consumer key and secret
  .setCallbackFunction('authCallback')
  .setPropertyStore(properties)
  
  .setConsumerKey(consumerKey)
  .setConsumerSecret(consumerSecret);
  
  this.baseUrl = 'https://api.twitter.com/1.1/';
};

Twitter.prototype.getService = function () {
  return this.service;
};

Twitter.prototype.get = function (path,params) {
  var url = this.baseUrl + path ;
  var options = {
    method: 'get',
    muteHttpExceptions: true
  };
  
  if (!_.isEmpty(params)) {
    url = url + "?" + 
    Object.keys(params).map(function(key) {
      return encodeRfc3986(key) + '=' + encodeRfc3986(params[key]);
    }).join('&');
  }
  
  var result = this.service.fetch(url);
  return JSON.parse(result.getContentText());
};

Twitter.prototype.post = function (path, payload) {
  var url = this.baseUrl + path ;
  var options = {
    method: 'post',
    payload: payload,
    muteHttpExceptions: true
  };
  var result = this.service.fetch(url,options);
  return JSON.parse(result.getContentText());
};

Twitter.prototype.getHomeTimeline = function(opt) {
  return this.get("statuses/home_timeline.json",opt);
};

Twitter.prototype.update = function(text) {
  return this.post("statuses/update.json",{status:text});
};

/**
* Encodes a string using the RFC 3986 spec.
*/
function encodeRfc3986(str) {
  return encodeURIComponent(str).replace(/[!'()]/g, function(char) {
    return escape(char);
  }).replace(/\*/g, "%2A");
}

